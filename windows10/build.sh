#!/usr/bin/env bash

echo " -> Building registry disk container..."
docker build -t registry.gitlab.com/tailoredlabs/os-images/windows10 .

echo " -> Pushing registry disk to Gitlab registry..."
docker push registry.gitlab.com/tailoredlabs/os-images/windows10
