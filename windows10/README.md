### Building a Windows image suitable for using inside kubevirt

### Slipstreaming VirtIO drivers into windows installation media (Inside Windows)

1. Acquire a Windows ISO file & the [https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/virtio-win.iso](virtIO driver ISO)
2. Extract the ISOs to the disk using [https://www.7-zip.org/](7zip)
3. Download & Install [https://www.ntlite.com/](ntlite)
4. Run ntlite, load the unpacked windows ISO into ntlite
5. Once the unpacked ISO has been loaded, the sidebar should change. Go to Integrate -> Drivers to show the drivers pane.
6. Click on Add, then navigate to the unpacked virtIO driver ISO directory and add everything inside.
7. Navigate to Finish -> Apply and select the option to create a new ISO. This new ISO will now contain the drivers QEMU requires to detect the virtual disks.

#### Build a disk for the VM

```
qemu-img create -f qcow2 win10.img 25G
```

#### Boot the VM with the streamlined Windows 10 ISO in it


Be sure to use acceleration. Use 'kvm' if you're on [Linux](https://wiki.archlinux.org/index.php/KVM), and 'hax' if you're on [Windows](https://www.qemu.org/2017/11/22/haxm-usage-windows/)

```
qemu-system-x86_64 \
  -accel kvm \ 
  -drive file=win10.img,if=virtio \
  -net nic -net user,hostname=windowsvm \
  -m 4G \
  -monitor stdio \
  -name "Windows" \
  -boot d \
-drive file=<windows 10 with slipstreamed virtIO drivers ISO file>,media=cdrom
```
#### Installation

Install windows as usual. Once windows is installed, win10.img is the harddrive that is used by kubernetes.

### Building a Docker image containing the OS disk

1. If you haven't already, login to the gitlab docker registry with `docker login registry.gitlab.com`.
2. Run `./build.sh`.
